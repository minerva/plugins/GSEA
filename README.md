# Gene Set Enrichment Analysis (GSEA) plugin

This is a plugin for GSEA in maps hosted in MINERVA. The plugin calculates enrichment for uploaded data overlays.
Public overlays are available for GSEA to all the users. User-provided (private) overlays can be analyzed only when a given user is logged in.  

## Load the plugin
Use the plugin icon to invoke the plugin popup, and paste the address of the plugin in the URL field (see figure below).
The address of the plugin is: `https://minerva-dev.lcsb.uni.lu/plugins/gsea/plugin.js`    
Press *UPLOAD* button to start the plugin.  


## Plugin functionality
To use the plugin, select from the left panel the overlay for which you would like to calculate the enrichment. 
Then, press the *Show enriched pathways* button.

After running the plugin you will:

1. See the results of the enrichment: the p-value is Bonferroni adjusted.
2The *Enriched area* column contains the names of pathways and submaps, for which are enriched in the considered overlay.
Enriched areas are highlighted in the map.
2. Have the possibility to download the tabular version of the enrichment results.

## Remarks
* The names of the pathways are not visible for the highlighted areas. Switch back to *Pathways and compartments* view to see the names.
* If multiple overlays are selected, the enrichment will be calculated for the union of the corresponding gene sets.
* The single elements behind the highlighted area are not clickable. Press *Reset* button to remove the highlighting.
