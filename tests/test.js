const {Builder, By, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const assert = require('chai').assert;

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

// Some tests need to access the MINERVA proxy to, e.g., check which elements are highlighted. However, the tests
// do not run in the same scope as the plugin and thus they do not have access to the Proxy. Therefore, the plugin
// exposes the proxy by attaching it as a data attribute to the main div element.
const pluginName = 'GSEA';
const pluginLabel = 'GSEA';
const minervaProxyContainerClass = pluginName + '-container';
const minervaProxyCode = `$('.${minervaProxyContainerClass}').data('minervaProxy')`;


function minervaLogin() {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("POST", 'http://localhost:8080/minerva/api/doLogin', false);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("login=admin&password=admin");
    });
}

async function getRequest(uri) {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("GET", uri);
        xhr.send();
    });
}

async function getPluginHash(){
    return getRequest('http://localhost:8080/minerva/api/plugins/').then(function (pluginsResponse) {
        let hashes = JSON.parse(pluginsResponse.responseText).filter(plugin => plugin.name === pluginLabel);
        if (hashes.length === 0){
            // when tested withing CI there is only one plugin, the current one and it's name is test
            hashes = JSON.parse(pluginsResponse.responseText);
        }
        return hashes[hashes.length -1].hash;
    });
}

describe('GSEA plugin', async function() {

    //Some functions can take a lot of time as they need, for isntance, start MINERVA interface
    this.timeout(40000);

    let driver;
    let minervaProxy;
    let pluginContainer;

    function wait(timeInMs) {
        return driver.executeAsyncScript(`
            const callback = arguments[arguments.length - 1];
            setTimeout(()=>callback(), ${timeInMs})`);
    }

    function deHighlightAll(){
        return driver.executeScript(`minervaProxy = ${minervaProxyCode}; minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) )`);
    }

    async function countHighlighted(){
        return driver.executeAsyncScript(`
                    var callback = arguments[arguments.length - 1];
                    ${minervaProxyCode}.project.map.getHighlightedBioEntities().then(highlighted => callback(highlighted));                    
                `);
    }

    before(async function () {
        const opts = new chrome.Options().addArguments('--no-sandbox', '--headless', '--remote-debugging-port=9222');
        driver = await new Builder().setChromeOptions(opts).forBrowser('chrome').build();
        // driver = await new Builder().forBrowser('chrome').build();

        await driver.manage().window().maximize();

        const loginResponse = await minervaLogin();
        const minervaToken = JSON.parse(loginResponse.responseText).token;

        await driver.get('http://localhost:8080');
        await driver.manage().addCookie({name: 'MINERVA_AUTH_TOKEN', value: minervaToken});
        const pluginHash = await getPluginHash();

        await driver.get(`http://localhost:8080/minerva/index.xhtml?id=single-map&plugins=${pluginHash}`);

        pluginContainer = await driver.wait(until.elementLocated(By.css(`.${minervaProxyContainerClass}`)));
        minervaProxy = await driver.executeScript(`return $('.${minervaProxyContainerClass}').data('minervaProxy')`);

        await driver.wait(until.elementLocated(By.css('.gsea-buttons .btn-calc')));
        await driver.executeScript(function () {
            $('td:contains("Sorted-by-species")').parent().find("input").click();
        });

        await wait(500);
    });

    describe("show enriched pathways", function () {
        before(async function () {
            const btn = driver.findElement(By.css('.gsea-buttons .btn-calc'));
            await btn.click();

            await driver.wait(until.elementLocated(By.css(`.gsea-results button`)));
        });

        let elPanelTitle;
        it('should have one record in the table being the main map', async function () {
            const panels = await driver.findElements(By.css('.gsea-pw-results-panel'));
            assert.equal(panels.length, 1);

            elPanelTitle = panels[0].findElement(By.css('.panel-title'));
            assert.equal(await elPanelTitle.getText(), 'Main map');
        });

        describe("clicking reset", function () {

            before(async function () {

                await driver.executeScript(function () {
                    $('.gsea-buttons .btn-reset').click();
                });

                await driver.wait(until.stalenessOf(elPanelTitle));
            });

            it("should reset results", async function () {
                const cnt = await driver.executeScript(function () {
                    return $('.gsea-results').children().length;

                });
                assert.equal(cnt, 0);
            })
        })
    });

    after(async function finishWebDriver() {
        await driver.quit();
    });
});