require('../css/styles.css');
require('../css/minerva.css');

const minervaWrapper = require('minerva-wrapper');
const MinervaElement = minervaWrapper.minerva.Element;
const Reaction = minervaWrapper.minerva.Element;

const pluginName = 'GSEA';
const pluginVersion = '1.0.0';
const containerName = pluginName + '-container';

const globals = {
  models: {},
  // selected: [],
  allBioEntities: [],
  hgncProteinRnaGene: {},
  allProteinRnaGene: [],
  modelBioentities: {},
  ModelPathway: {},
  // quickAliases: []
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let minervaVersion;

let $ = window.$;


const register = function (_minerva) {

  console.log('registering ' + pluginName + ' plugin');

  $(".tab-content").css('position', 'relative');

  pluginContainer = $(minervaWrapper.getElement());

  pluginContainerId = pluginContainer.attr('id');
  if (!pluginContainerId) {
    //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
    pluginContainer.css('height', '100%');
    pluginContainerId = pluginContainer.parent().attr('id');
  }

  console.log('minerva object ', minervaWrapper);
  console.log('project id: ', minervaWrapper.getProjectId());

  return minervaWrapper.getModels().then(function (models) {
    console.log('model id: ', models[0].getId());
    models.forEach(m => globals.models[m.getId()] = m);
    return minervaWrapper.getVersion();
  }).then(function (version) {
    minervaVersion = parseFloat(version.split('.').slice(0, 2).join('.'));
    const container = initMainContainer();
    calculatePathways().then(function () {
      container.find('.gsea-pw-loading').hide();
      initMainPageStructure(container);
      pluginContainer.find(`.${containerName}`).data("minervaProxy", minervaProxy);
    });
  });
};

const unregister = async function () {
  console.log('unregistering ' + pluginName + ' plugin');

  await unregisterListeners();
  await deHighlightAll();
};

minervaWrapper.init({
  register: register,
  unregister: unregister,
  name: pluginName,
  version: pluginVersion,
  minWidth: 500,
  pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/gsea/plugin.js',
});


function initPlugin() {
  initMainPageStructure();
}

async function unregisterListeners() {
  await minervaWrapper.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************

function isReaction(bioEntity) {
  return !("_compartmentId" in bioEntity);
}

function deHighlightAll() {
  pluginContainer.find('.gsea-results').empty();
  return minervaWrapper.getHighlightedBioEntities().then(highlighted => {
    return minervaWrapper.hideBioEntity(highlighted.map(function (entry) {
      return {
        element: {
          id: entry.element.getId(),
          modelId: entry.element.getModelId(),
          type: isReaction(entry.element) ? "REACTION" : "ALIAS"
        },
        type: entry.type,
        bioEntity: entry.element,
      }
    }));
  });
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************


function getContainerClass() {
  return containerName;
}

function initMainContainer() {
  const container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
  container.append(`
    <div class="modal fade modal-container" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
    </div>
    <div class="panel card panel-default gsea-pw-loading">
        <div class="panel-body card-body">  
            <i class="fa fa-circle-o-notch fa-spin"></i> Obtaining pathway-gene mapping from the map
        </div>        
    </div>`);

  return container;
}


function initMainPageStructure() {

  const container = $("." + getContainerClass());//.appendTo(pluginContainer);
  container.append(`       
        <div class="panel card panel-primary border-primary panel-events">
            <div class="panel-heading card-header bg-primary text-light">Gene Set Enrichment Analysis</div>
            <div class="panel-body card-body gsea-main-panel">
                <div class="gsea-buttons">
                    <div class="row">
                        <div class="col-sm-8">
                            <button type="button" class="btn-calc btn btn-success btn-default btn-block">Show enriched pathways</button>            
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn-reset btn btn-success btn-default btn-block">Reset</button>
                        </div>
                    </div>
                </div>
                <div class="gsea-results">
                </div>
            </div>
        </div>
        
        <div class="panel card panel-info border-info gsea-instructions">
            <div class="panel-heading card-header bg-info text-light" data-toggle="collapse" data-target="#instructions_panel_${pluginContainerId}">
                Instructions
                <span class="pull-right fa fa-chevron-down">
            </div>
            <div id="instructions_panel_${pluginContainerId}" class="panel-body card-body panel-default panel-collapse collapse">                
                This is a basic Gene Set Enrichment plugin. It calculates enrichment of elements from selected overlay(s) in the pathways of the map (coloured areas).<br><br>
                <u>Show enriched pathways</u> button calculates the enrichment scores (Bonferroni-adjusted p-value), lists the significantly enriched pathways in the window "Gene Set Enrichment Analysis" window, and highlights them.<br><br>
                <u>Reset</u> button removes the results and the enrichment.<br><br>
                If multiple overlays are selected, the enrichment will be calculated for the combined gene sets.                        
            </div>                 
        </div>
        
    `);


  container.find('.btn-calc').on('click', () => calculateGSEA());
  container.find('.btn-reset').on('click', () => deHighlightAll());
}

function pointInRectangle(x, y, rectangle) {
  return x >= rectangle[0] && x <= rectangle[1] &&
    y >= rectangle[2] && y <= rectangle[3];
}

function calculatePathways() {

  function populatePathways() {

    const pwIdName = {};
    const modelHgncs = {};
    const modelAllProteinRnaGene = {};
    globals.allBioEntities.forEach(e => {

      const eType = e.getType();
      const modelId = e.getModelId();

      if (!(modelId in modelAllProteinRnaGene)) {
        modelAllProteinRnaGene[modelId] = [];
      }

      if (!(modelId in globals.ModelPathway)) {
        globals.ModelPathway[modelId] = {};
        //empty pathway name stands for the whole submap
        globals.ModelPathway[modelId][''] = {
          bioEntities: [],
          hgncNames: new Set(),
          pVal: undefined
        };
        modelHgncs[modelId] = [];
      }

      if (eType === "Pathway") {
        pwIdName[e.id] = e.getName();
        const pwName = e.getName();
        if (!(pwName in globals.ModelPathway[modelId])) {
          globals.ModelPathway[modelId][pwName] = {
            bioEntities: [e],
            hgncNames: new Set(),
            pVal: undefined
          }
        } else {
          globals.ModelPathway[modelId][pwName].bioEntities.push(e);
        }
      } else if (eType === "Protein" || eType === "RNA" || eType === "Gene") {
        globals.allProteinRnaGene.push(e);
        modelAllProteinRnaGene[modelId].push(e);
        const hgncs = [...new Set(e.getReferences().filter(ref => ref.getType() === "HGNC_SYMBOL").map(ann => ann.getResource()))];
        globals.hgncProteinRnaGene[e.id] = hgncs;

        if (hgncs.length > 0) {
          //empty pathway name stands for the whole (sub)map
          modelHgncs[modelId] = modelHgncs[modelId].concat(hgncs);
        }
      }
    });

    Object.keys(globals.ModelPathway).forEach(modelId => {

      //hgncs might have been inserted multiple times
      globals.ModelPathway[modelId][''].hgncNames = new Set(modelHgncs[modelId]);

      Object.keys(globals.ModelPathway[modelId]).forEach(pwName => {
        if (pwName === "") return; //this situation is handled above

        let hgncs = [];
        globals.ModelPathway[modelId][pwName].bioEntities.forEach(e => {
          const pwDims = [e.getX(), e.getX() + e.getWidth(), e.getY(), e.getY() + e.getHeight()];
          modelAllProteinRnaGene[modelId].forEach(prg => {
            // globals.allProteinRnaGene.forEach(prg => {
            if (pointInRectangle(prg.getX(), prg.getY(), pwDims) ||
              pointInRectangle(prg.getX() + prg.getWidth(), prg.getY(), pwDims) ||
              pointInRectangle(prg.getX(), prg.getY() + prg.getHeight(), pwDims) ||
              pointInRectangle(prg.getX() + prg.getWidth(), prg.getY() + prg.getHeight(), pwDims)) {
              hgncs = hgncs.concat(globals.hgncProteinRnaGene[prg.id]);
            }
          });
        });
        globals.ModelPathway[modelId][pwName].hgncNames = new Set(hgncs);
      });
    });
  }

  return minervaWrapper.getAllBioEntities().then(function (bioEntities) {
    globals.allBioEntities = bioEntities;
    populatePathways();
  });

}

function calculateGSEA() {
  deHighlightAll().then(function () {
    return minervaWrapper.getVisibleDataOverlays()
  }).then(function (overlays) {

    if (overlays.length === 0) {
      alert("No overlay selected");
      return;
    }

    let ovHgncNames = new Set();
    overlays.forEach(e => {
      // e is a LayoutAlias, needs to be mapped back to map aliases, to retrieve HGNC; defined as set, .has() function looks better in filtering
      const these_ovaliases = new Set([...e.getAliases().map(g => g.getId())]);
      // grab aliases matching the overlay -> get their references -> filter by HGNC_SYMBOL -> get value
      const these_ovnames = new Set(globals.allProteinRnaGene.filter(prg => these_ovaliases.has(prg.getId())).flatMap(ref => globals.hgncProteinRnaGene[ref.id]));

      ovHgncNames = new Set([...ovHgncNames, ...these_ovnames]);
    });


    const highlightPathways = [];

    let cntPVals = 0;
    Object.keys(globals.ModelPathway).forEach(modelId => {
      Object.keys(globals.ModelPathway[modelId]).forEach(pwName => {
        const pw = globals.ModelPathway[modelId][pwName];
        const mpHgncNames = pw.hgncNames;

        //calculate intersect
        const isect = [...ovHgncNames].filter(name => mpHgncNames.has(name));

        var m = mpHgncNames.size; //number of entities in the pathway (with HGNC_SYMBOL)
        var k = isect.length; //number of entities in both pathway and overlay (with HGNC_SYMBOL)
        var N = globals.allProteinRnaGene.length; // number of all entities (with HGNC_SYMBOL)
        var n = ovHgncNames.size; //number of entities in the overlay (with HGNC_SYMBOL)

        console.log('pw', pwName);
        console.log('m', m);
        console.log('k', k);
        console.log('N', N);
        console.log('n', n);

        if (k > 0) {
          //var px = (kcomb(m,k)*kcomb(N-m,n-k))/kcomb(N,n);
          //Approximation using upper boud from Eq (2) in http://page.mi.fu-berlin.de/shagnik/notes/binomials.pdf
          //Simplified, props to Ewa Smula (ewa.smula@uni.lu)
          pw.pVal = ({
            pVal: Math.pow(((m * (n - k)) / (k * (N - m))), k) * Math.pow(((n * (N - m)) / (N * (n - k))), n),
            pw: pw,
            pwModelId: modelId
          });
          cntPVals += 1;
        } else {
          pw.pVal = undefined;
        }
      });
    });

    const dataForExport = {
      overlays: overlays.map(e => e.getName()),
      mapProjectId: minervaWrapper.getProjectId(),
      mapApi: minervaWrapper.getBaseUrl(),
      data: []
    };


    Object.keys(globals.ModelPathway).forEach(modelId => {

      let pVals = Object.keys(globals.ModelPathway[modelId])
        .filter(pwName => globals.ModelPathway[modelId][pwName].pVal !== undefined)
        .map(pwName => globals.ModelPathway[modelId][pwName].pVal);

      pVals.forEach(p => p.adjustedPVal = Math.min(p.pVal * cntPVals, 1));
      pVals = pVals.filter(pv => pv.adjustedPVal < 0.1);

      if (pVals.length === 0) {
        return;
      }

      let mapName = globals.models[modelId].name;
      if (!mapName) mapName = "Main map";

      let panelTitle;
      if (minervaVersion < 14) {
        panelTitle = `<h3 class="panel-title">${mapName}</h3>`;
      } else {
        panelTitle = `<span class="panel-title">${mapName}</span>`;
      }

      const $mapResultsPanel = $(`
                <div class="panel card panel-primary border-primary gsea-pw-results-panel">
                    <div class="panel-heading card-header bg-primary text-light map-results">
                        ${panelTitle}
                    </div>
                    <div class="panel-body card-body">                        
                        <table class="gsea-results-table">
                            <thead>
                            <tr><th class="first-column" title="Bonferroni-adjusted p-value ">p-val (adj)</th><th>Enriched area</th>                            
                            </thead>
                            
                        </table>
                    </div>                    
                </div>
            `).appendTo(pluginContainer.find('.gsea-results'));
      const $table = $mapResultsPanel.find('table');

      const exportVals = [];

      pVals.sort(function (a, b) {
        return a.adjustedPVal - b.adjustedPVal;
      }).forEach(p => {
        const pValText = p.adjustedPVal < 0.0001 ? "< 0.0001" : `${p.adjustedPVal.toFixed(4)}`;
        if (p.pw.bioEntities.length === 0) {
          // For entire diagrams (empty pathway names) add them to the results table
          $table.append(`<tr><td class="first-column">${pValText}</td><td>Entire diagram</td></tr>`);
          exportVals.push({enrichedArea: "Entire diagram", adjustedPVal: p.adjustedPVal});
          return;
        }
        const pwName = p.pw.bioEntities[0].getName();

        $table.append(`<tr><td class="first-column">${pValText}</td><td>${pwName}</td></tr>`);
        exportVals.push({enrichedArea: pwName, adjustedPVal: p.adjustedPVal});

        p.pw.bioEntities.forEach(e => {
          highlightPathways.push({
            element: {
              id: e.getId(),
              modelId: e.getModelId(),
              type: e.constructor.name.toUpperCase()
            },
            type: "SURFACE",
            options: {
              color: '#00FF00',
              opacity: 0.1
            },
            bioEntity: e
          });
        });

      });
      dataForExport.data.push({mapName: mapName, areas: exportVals})
    });
    addExport(pluginContainer.find('.gsea-results'), dataForExport);
    return minervaWrapper.showBioEntity(highlightPathways);
  });
}

function addExport($container, data) {

  $container.append($(`<div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="btn-calc btn btn-success btn-default btn-block">Export</button>            
                            </div>
                        </div>`));

  $container.find("button").on("click", () => exportData(data, $container));
}

const exportData = function (data, $container) {
  openModal({
    body:
      '<button type="button" class="btn btn-block ceButtonExportJSON">JSON</button>' +
      '<button type="button" class="btn btn-block ceButtonExportTSV">TSV</button>'
  });

  pluginContainer.find('.ceButtonExportJSON').on('click', () => exportToFormat('json', data, $container));
  pluginContainer.find('.ceButtonExportTSV').on('click', () => exportToFormat('tsv', data, $container));
};

const exportToFormat = function (format, data, $container) {

  let exportString;
  if (format === "json") exportString = exportToJson(data);
  if (format === "tsv") exportString = exportToTsv(data);

  let fileName = 'export.' + format;
  let blob = new Blob([exportString], {type: 'text/' + format + ';charset=utf-8;'});
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, fileName);
  } else {
    let link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  closeModal();
};

function closeModal() {

  pluginContainer.find(".modal-container").modal("hide");
}

function openModal(params) {

  let modal = pluginContainer.find(".modal-container");
  let modalContent = modal.find(".modal-content");
  modalContent.empty();
  let modalContentClass = "modal-content";
  if ("modalClass" in params) modalContentClass += " " + params.modalClass;
  modalContent.attr("class", modalContentClass);
  if ("header" in params) {
    let header = pluginContainer.find('<div class="modal-header panel-heading"></div>').appendTo(modalContent);
    header.append('<h4>' + params.header + '</h4>');
    header.append('<button type="button" class="close">&times;</button>');

  }
  if ("body" in params) {
    let body = $('<div class="modal-body"></div>').appendTo(modalContent);
    body.append(params.body);
  }
  // '           <div class="modal-footer">' +
  // '               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
  // '           </div>' +

  modal.modal("show");
}

function exportToTsv(data) {

  let res = `# overlays: ${data.overlays}; mapProjectId: ${data.mapProjectId}; mapApi: ${data.mapApi}\n`;

  for (let i = 0; i < data.data.length; ++i) {
    res += `${data.data[i].mapName}\n`;
    res += '"Enriched area"\t"p-val (adj)"\n';
    const areas = data.data[i].areas;
    for (let j = 0; j < areas.length; ++j) {
      res += `"${areas[j].enrichedArea}"\t"${areas[j].adjustedPVal}"\n`
    }
    res += '\n'
  }
  return res;
}

function exportToJson(data) {

  return JSON.stringify(data, null, 2);
}
